#define BLYNK_PRINT Serial
#define BLYNK_MAX_READBYTES 1024

#include <SPI.h>
#include <WiFiNINA.h>
#include <BlynkSimpleWiFiNINA.h>
#include <TimeLib.h>
#include <Regexp.h>
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include "Adafruit_ImageReader.h"
#include <SD.h>

#include <Pushbutton.h>
#include "constants.h"

char auth[] = "3dc47f4311914066859c546927fb9b35";
char ssid[] = "pogui";
char pass[] = "pibedeoro";

char weather_api_id[] = "ed3a6324ac8fdfa5e3c227512d5c7f12";

/*
 * Menu Button
 */
int menu_id = 0;
int prev_menu_id = 0;
boolean menu_change = true;

#define MENU_BUTTON_PIN 5
Pushbutton menu_button(MENU_BUTTON_PIN);

#define MENU_COUNTDOWN 0
#define MENU_CLOCK 1
#define MENU_WEATHER 2
#define MENU_SLIDESHOW 3
#define MENU_N_MENUS 4

int menu_timers[MENU_N_MENUS];

/*
 * Option Button
 */
#define OPTION_BUTTON_PIN 6
Pushbutton option_button(OPTION_BUTTON_PIN);

/*
 * Timer
 */
BlynkTimer timer;

/*
 * TFT Screen
 */
#define TFT_DC  9
#define TFT_CS  10
#define SD_CS   4
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
Adafruit_ImageReader image_reader;

/*
 * LED
 */
#define LED_PIN 2

/*
 * Status Variables
 */
bool refresh_screen = true;

WeatherData weather_data;
int current_timezone = 0;

int slideshow_current_image = 1;

bool countdown_buzzer = true;

void setup()
{
  Serial.begin(9600); // Debug
  Blynk.begin(auth, ssid, pass);
  
  menu_timers[MENU_CLOCK] = timer.setInterval(1000, menu_clock);
  menu_timers[MENU_WEATHER] = timer.setInterval(1000, menu_weather);
  menu_timers[MENU_SLIDESHOW] = timer.setInterval(8000, menu_slideshow);
  menu_timers[MENU_COUNTDOWN] = timer.setInterval(1000, menu_countdown);
  
  timer.disable(menu_timers[MENU_CLOCK]);
  timer.disable(menu_timers[MENU_WEATHER]);
  timer.disable(menu_timers[MENU_SLIDESHOW]);
  timer.disable(menu_timers[MENU_COUNTDOWN]);

  pinMode(2, OUTPUT);
  
  tft.begin();
  tft.setRotation(1);
  tft.fillScreen(ILI9341_BLUE);
  if(!SD.begin(SD_CS)) {
    Serial.println(F("SD Begin Failure"));
  }
}

BLYNK_CONNECTED() {
  update_date_time();
}

void loop()
{   
  Blynk.run();
  timer.run();

  if (menu_change) {
    //Serial.print("Menu ID: "); Serial.println(menu_id);

    timer.disable(menu_timers[prev_menu_id]);
    timer.enable(menu_timers[menu_id]);

    if (menu_id == MENU_WEATHER) {
      update_weather_data();  
    }

    if (menu_id == MENU_CLOCK) {
      update_date_time();  
    }

    menu_change = false;
    refresh_screen = true;
  }

  if (menu_button.getSingleDebouncedRelease()) {
    prev_menu_id = menu_id;
    menu_id = (menu_id == MENU_N_MENUS - 1) ? 0 : menu_id + 1;
    menu_change = true;
  }

  if (option_button.getSingleDebouncedRelease()) {
    if (menu_id == MENU_CLOCK) {
      menu_clock_option();
    } else if (menu_id == MENU_WEATHER) {
      menu_weather_option();
    } else if (menu_id == MENU_COUNTDOWN) {
      countdown_buzzer = !countdown_buzzer;
    }
  }
}

/*
 * Menu Functions
*/
void menu_clock() {
  int a;
  String current_time = "";
  String current_date = "";

  a = hour();
  if (a < 10) { current_time += "0"; }
  current_time += a;
  current_time += ":";

  a = minute();
  if (a < 10) { current_time += "0"; }
  current_time += a;
  
  current_date += WEEKDAYS[weekday() - 1];
  current_date += " ";

  a = day();
  if (a < 10) { current_date += "0"; }
  current_date += a;
  current_date += "/";

  a = month();
  if (a < 10) { current_date += "0"; }
  current_date += a;
  current_date += "/";

  current_date += year();

  if (second() == 0) {
    refresh_screen = true;
  }
  
  if (refresh_screen) {
    int l, x, y;
    
    tft.fillScreen(ILI9341_BLACK);
    tft.setTextColor(ILI9341_WHITE);

    l = current_time.length();
    l = ((l * FONT_W) + (l - 1)) * TIME_FONT_SIZE;
    x = (tft.width() - l) / 2;
    y = (tft.height() - ((FONT_H * TIME_FONT_SIZE) + CLOCK_LINE_H + (FONT_H * DATE_FONT_SIZE) + CLOCK_LINE_H + CLOCK_LINE_H + (FONT_H * TIMEZONE_FONT_SIZE))) / 2;
    tft.setCursor(x, y);
    tft.setTextSize(TIME_FONT_SIZE);
    tft.println(current_time);

    l = current_date.length();
    l = ((l * FONT_W) + (l - 1)) * DATE_FONT_SIZE;
    x = (tft.width() - l) / 2;
    y = y + (FONT_H * TIME_FONT_SIZE) + CLOCK_LINE_H;
    tft.setCursor(x, y);
    tft.setTextSize(DATE_FONT_SIZE);
    tft.println(current_date);

    l = TIMEZONES[current_timezone].clock_display.length();
    l = ((l * FONT_W) + (l - 1)) * TIMEZONE_FONT_SIZE;
    x = (tft.width() - l) / 2;
    y = y + (FONT_H * DATE_FONT_SIZE) + CLOCK_LINE_H + CLOCK_LINE_H;
    tft.setCursor(x, y);
    tft.setTextSize(TIMEZONE_FONT_SIZE);
    tft.println(TIMEZONES[current_timezone].clock_display);
    
    refresh_screen = false;

//    Serial.println(current_time);
//    Serial.println(current_date);
//    Serial.println(TIMEZONES[current_timezone].clock_display);
//    Serial.println();
  }
}

void menu_clock_option() {
  current_timezone += 1;
  if (current_timezone == N_TIMEZONES) {
    current_timezone = 0;
  }

  update_date_time();
}

void menu_weather() {
  //Serial.println("Weather Menu");
  
  if (refresh_screen) {
    String  icon_tmp = "/" + weather_data.icon + ".bmp";
    int     l = icon_tmp.length() + 1;
    char    icon_file[l];
    int     x, y;
    String  s;

    icon_tmp.toCharArray(icon_file, l);
    image_reader.drawBMP(icon_file, tft, 0, 0);

    tft.setTextColor(ILI9341_BLACK);

    s = String(weather_data.temp);
    l = s.length();
    l = ((l * FONT_W) + (l - 1)) * WEATHER_TEMP_FONT_SIZE;
    x = WEATHER_ICON_W + ((tft.width() - WEATHER_ICON_W - l) / 2);
    y = WEATHER_PADDING_TOP;
    tft.setCursor(x - (WEATHER_DEG_R / 2), y);
    tft.setTextSize(WEATHER_TEMP_FONT_SIZE);
    tft.println(s);
    tft.fillCircle(x + l + WEATHER_DEG_R + WEATHER_DEG_R, y + WEATHER_DEG_R, WEATHER_DEG_R, ILI9341_BLACK);

    s = String(weather_data.temp_min) + " - " + String(weather_data.temp_max);
    l = s.length();
    l = ((l * FONT_W) + (l - 1)) * WEATHER_TEMP_M_FONT_SIZE;
    x = WEATHER_ICON_W + ((tft.width() - WEATHER_ICON_W - l) / 2);
    y = WEATHER_PADDING_TOP + (FONT_H * WEATHER_TEMP_FONT_SIZE) + WEATHER_TEMP_LINE_H;
    tft.setCursor(x, y);
    tft.setTextSize(WEATHER_TEMP_M_FONT_SIZE);
    tft.println(s);

    l = weather_data.description.length();
    l = ((l * FONT_W) + (l - 1)) * WEATHER_DESC_FONT_SIZE;
    x = (tft.width() - l) / 2;
    y = tft.height() - WEATHER_PADDING_BOTTOM - (FONT_H * WEATHER_TIMEZONE_FONT_SIZE) - (WEATHER_TEMP_LINE_H / 2) - (FONT_H * WEATHER_TIMEZONE_FONT_SIZE);
    tft.setCursor(x, y);
    tft.setTextSize(WEATHER_DESC_FONT_SIZE);
    tft.println(weather_data.description);
    
    l = TIMEZONES[current_timezone].clock_display.length();
    l = ((l * FONT_W) + (l - 1)) * WEATHER_TIMEZONE_FONT_SIZE;
    x = (tft.width() - l) / 2;
    y = tft.height() - (FONT_H * WEATHER_TIMEZONE_FONT_SIZE) - WEATHER_PADDING_BOTTOM;
    tft.setCursor(x, y);
    tft.setTextSize(WEATHER_TIMEZONE_FONT_SIZE);
    tft.println(TIMEZONES[current_timezone].clock_display);

    refresh_screen = false;
  }
}

void menu_weather_option() {
  current_timezone += 1;
  if (current_timezone == N_TIMEZONES) {
    current_timezone = 0;
  }

  update_weather_data();
}

void menu_slideshow() {
  String          tmp = "/" + String(slideshow_current_image) + ".bmp";
  int             l = tmp.length() + 1;
  char            filename[l];
  ImageReturnCode res;
  
  //Serial.println("Slideshow Menu");
  
  tmp.toCharArray(filename, l);
  res = image_reader.drawBMP(filename, tft, 0, 0);
  //Serial.println(filename);
  image_reader.printStatus(res);

  slideshow_current_image += 1;
  if (slideshow_current_image > N_SLIDESHOW_IMAGES) {
    slideshow_current_image = 1;
  }
}

void menu_countdown() {
  //Serial.println("Countdown Menu");  
  time_t  delivery_time = 1578571200;
  String  s;
  int     x, y, l;
  static bool light_status = true;

  if (countdown_buzzer) {
    tone(13, 1000, 500);
    if (light_status) {
      digitalWrite(LED_PIN, HIGH);
    } else {
      digitalWrite(LED_PIN, LOW);
    }
    light_status = !light_status;
  } else {
    digitalWrite(LED_PIN, LOW);
  }

  tft.fillScreen(ILI9341_BLACK);
  tft.setTextColor(ILI9341_WHITE);
  
  s = String("Delivery in...");
  l = s.length();
  l = ((l * FONT_W) + (l - 1)) * COUNTDOWN_TEXT_FONT_SIZE;
  x = (tft.width() - l) / 2;
  y = (tft.height() - ((FONT_H * COUNTDOWN_TEXT_FONT_SIZE) + (FONT_H * COUNTDOWN_FONT_SIZE) + COUNTDOWN_LINE_H)) / 2;
  tft.setCursor(x, y);
  tft.setTextSize(COUNTDOWN_TEXT_FONT_SIZE);
  tft.println(s);

  delivery_time = (delivery_time - now());
  s = String(delivery_time) + "s";
  
  l = s.length();
  l = ((l * FONT_W) + (l - 1)) * COUNTDOWN_FONT_SIZE;
  x = (tft.width() - l) / 2;
  y = (tft.height() - ((FONT_H * COUNTDOWN_TEXT_FONT_SIZE) + (FONT_H * COUNTDOWN_FONT_SIZE) + COUNTDOWN_LINE_H)) / 2  + ((FONT_H * COUNTDOWN_TEXT_FONT_SIZE) + COUNTDOWN_LINE_H);
  tft.setCursor(x, y);
  tft.setTextSize(COUNTDOWN_FONT_SIZE);
  tft.println(s);
}

/*
 * Application Interaction
*/
void update_date_time() {
  Blynk.virtualWrite(V30, TIMEZONES[current_timezone].clock_api);
  digitalWrite(LED_PIN, HIGH);
}

BLYNK_WRITE(V30) {
  MatchState    ms;
  char          x;
  
  String        response = param.asStr();
  int           l = response.length() + 1;
  char          match_str[l];
  int           unix_time;
  int           offset;

  response.toCharArray(match_str, l);
  ms.Target(match_str);
  x = ms.Match("datetime: (.-)\n");

  if (x > 0) {  
    Serial.println(response.substring(ms.MatchStart, ms.MatchStart + ms.MatchLength - 1));
    // datetime: 2019-07-20T20:05:43.945922+02:00
    
    int start_idx = ms.MatchStart;
    int y = response.substring(start_idx + 10, start_idx + 10 + 4).toInt();
    int m = response.substring(start_idx + 15, start_idx + 15 + 2).toInt();
    int d = response.substring(start_idx + 18, start_idx + 18 + 2).toInt();
    int h = response.substring(start_idx + 21, start_idx + 21 + 2).toInt();
    int mn = response.substring(start_idx + 24, start_idx + 24 + 2).toInt();
    int s = response.substring(start_idx + 27, start_idx + 27 + 2).toInt();
    
    setTime(h, mn, s, d, m, y);
    refresh_screen = true;
  } else {
    Serial.println("Timezone API failure");
  }
  digitalWrite(LED_PIN, LOW);
}

void update_weather_data() {  
  Blynk.virtualWrite(
    V31,
    String(TIMEZONES[current_timezone].lat),
    String(TIMEZONES[current_timezone].lng),
    String(weather_api_id)
  );
  digitalWrite(LED_PIN, HIGH);
}

BLYNK_WRITE(V31) {
  MatchState  ms;
  char        x;
  
  String      response = param.asStr();
  int         l = response.length() + 1;
  char        match_str[l];
  
  response.toCharArray(match_str, l);
  ms.Target(match_str);
  x = ms.Match("\"timezone\":%-?%d+");

  if (x > 0) {   
    //{"coord":{"lon":-17.47,"lat":14.72},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}],"base":"stations","main":{"temp":26,"pressure":1013,"humidity":74,"temp_min":26,"temp_max":26},"visibility":8000,"wind":{"speed":3.6,"deg":270},"clouds":{"all":27},"dt":1563660292,"sys":{"type":1,"id":2410,"message":0.0097,"country":"SN","sunrise":1563605372,"sunset":1563651751},"timezone":0,"id":8030383,"name":"Mermoz Boabab","cod":200}
    int     offset;
    String  z = "";

    z = response.substring(ms.MatchStart + 11, ms.MatchStart + ms.MatchLength);
    offset = z.toInt();
    //Serial.print("Offset: "); Serial.println(z);

    x = ms.Match("\"sunrise\":%d+");
    z = response.substring(ms.MatchStart + 10, ms.MatchStart + ms.MatchLength);
    weather_data.sunrise = string_to_time_t(z) + offset;
    //Serial.print("Sunrise: "); Serial.println(z);

    ms.Match("\"sunset\":%d+");
    z = response.substring(ms.MatchStart + 9, ms.MatchStart + ms.MatchLength);
    weather_data.sunset= string_to_time_t(z) + offset;
    //Serial.print("Sunset: "); Serial.println(z);

    //ms.Match("\"main\":\"[%a%s]+");
    //weather_data.main = response.substring(ms.MatchStart + 8, ms.MatchStart + ms.MatchLength);
    //Serial.print("Main: "); Serial.println(weather_data.main);

    ms.Match("\"description\":\"[%a%s]+");
    weather_data.description = response.substring(ms.MatchStart + 15, ms.MatchStart + ms.MatchLength);
    //Serial.print("Description: "); Serial.println(weather_data.description);

    ms.Match("\"icon\":\"[%w]+");
    weather_data.icon = response.substring(ms.MatchStart + 8, ms.MatchStart + ms.MatchLength);
    //Serial.print("Icon: "); Serial.println(weather_data.icon);
    
    ms.Match("\"temp\":[%d]+");
    weather_data.temp = response.substring(ms.MatchStart + 7, ms.MatchStart + ms.MatchLength);
    //Serial.print("Temp: "); Serial.println(weather_data.temp);

    ms.Match("\"temp_min\":[%d]+");
    weather_data.temp_min = response.substring(ms.MatchStart + 11, ms.MatchStart + ms.MatchLength);
    //Serial.print("Temp Min: "); Serial.println(weather_data.temp_min);

    ms.Match("\"temp_max\":[%d]+");
    weather_data.temp_max = response.substring(ms.MatchStart + 11, ms.MatchStart + ms.MatchLength);
    //Serial.print("Temp Max: "); Serial.println(weather_data.temp_max);

    //ms.Match("\"humidity\":[%d]+");
    //weather_data.humidity = response.substring(ms.MatchStart + 11, ms.MatchStart + ms.MatchLength);
    //Serial.print("Humidity: "); Serial.println(weather_data.humidity);
    
    //String f = String(hour(weather_data.sunrise)) + ":" + minute(weather_data.sunrise) + ":" + second(weather_data.sunrise);
    //Serial.print("Sunrise (f): "); Serial.println(f);

    //Serial.println(response);
    refresh_screen = true;
  } else {
    Serial.println("Weather API failure");
  }
  digitalWrite(LED_PIN, LOW);
}

time_t string_to_time_t(String s) {
  time_t r = 0;
  
  for (int i = 0; s[i] != '\0'; i++) {
    r = r* 10 + s[i] - '0';
  }

  return r;
}
