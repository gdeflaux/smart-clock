#define FONT_W  5
#define FONT_H  7

/*
 * Clock Menu
 */
#define TIME_FONT_SIZE              8
#define DATE_FONT_SIZE              3
#define TIMEZONE_FONT_SIZE          2
#define CLOCK_LINE_H                5

/*
 * Timezone
 */
String WEEKDAYS[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

class Timezone {
  public:
  Timezone(String _clock_display, String _clock_api, float _lat, float _lng) {
     clock_display = _clock_display;
     clock_api = _clock_api;
     lng = _lng;
     lat = _lat;
  }
  
  String  clock_display;
  String  clock_api;
  float   lng, lat;
};

Timezone TIMEZONES[] = {
  { "Dakar", "/Africa/Dakar", 14.716677, -17.467686 },
  { "Cape Town", "/Africa/Johannesburg", -33.918861, 18.423300 },
  { "Lyon", "/Europe/Paris", 45.763420, 4.834277 },
  { "Boston", "/America/New_York",  40.73, -73.99 }
};
#define N_TIMEZONES 4

/*
 * Weather
 */
class WeatherData {
  public:    
    time_t  sunrise = 0;
    time_t  sunset = 0;
    String  main, description, icon = "01d";
    String  temp, temp_min, temp_max, humidity;
};

#define WEATHER_TIMEZONE_FONT_SIZE        3
#define WEATHER_TEMP_FONT_SIZE            8
#define WEATHER_TEMP_M_FONT_SIZE          2
#define WEATHER_DESC_FONT_SIZE            2
#define WEATHER_TEMP_LINE_H               10
#define WEATHER_DEG_R                     5
#define WEATHER_ICON_W                    180
#define WEATHER_PADDING_TOP               70
#define WEATHER_PADDING_BOTTOM            5

/*
 * Slideshow
 */

#define N_SLIDESHOW_IMAGES  27

/*
 * Countdown
 */
#define COUNTDOWN_TEXT_FONT_SIZE  2
#define COUNTDOWN_FONT_SIZE       4 
#define COUNTDOWN_LINE_H          40
